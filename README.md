### diff = diffoscope zipflinger-7.2.2.jar  zipflinger_lb.jar > diff

### diff2 = diffoscope zipflinger_lb.jar zipflinger-7.2.2.jar  > diff2

### diff11 = diffoscope zipflinger-7.2.2.jar  zipflinger.jar > diff11

### diff22 = diffoscope zipflinger.jar zipflinger-7.2.2.jar > diff22

### lbs.diff = diffoscope zipflinger.jar  zipflinger_lb.jar > lbs.diff ( diffing the local builds from the two different sources listed below)

> zipflinger-7.2.2.jar is the official jar <br />
> zipflinger_lb.jar locally built jar, source: https://android.googlesource.com/platform/tools/base/+/refs/heads/mirror-goog-studio-master-dev/zipflinger/<br />
> zipflinger.jar locally built, source: https://android.googlesource.com/platform/tools/base/+/refs/tags/studio-2021.2.1-patch2/zipflinger/
